<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'login';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

// Dashboard
$route['Dashboard'] = 'Dashboard';
$route['Dashboard/Userktp'] = 'Dashboard/ktpel';
$route['Dashboard/Ktpel'] = 'Dashboard_ktpel';
$route['Dashboard/Biodata'] = 'Dashboard_biodata';
$route['Dashboard/Mobilitas'] = 'Dashboard_mobilitas';
$route['Dashboard/Capil'] = 'Dashboard_capil';

// Master Menu
$route['Master/MyActivity'] = 'Activity';
$route['Master/Activity'] = 'Master_activity';
$route['Master/List'] = 'Master_list';
$route['Master/Request'] = 'Master_pengajuan';
$route['Master/Contact'] = 'Contact';

// Check Ktp-el
$route['Check/Ktpel'] = 'Nik_cetak';
$route['Check/Ktpel/Export'] = 'Nik_cetak/export';
$route['Check/Ktpel/Pdf'] = 'Nik_cetak/pdf';
$route['Check/Ktpel/Push'] = 'Nik_cetak/do_push';
$route['Check/History'] = 'Nik_history';
$route['Check/Duplicate'] = 'Nik_duplicate';
$route['Check/Duplicate/Export'] = 'Nik_duplicate/export';
$route['Check/Prr'] = 'Nik_prr';
$route['Check/Suket'] = 'Nik_prsuket';
$route['Check/Rekam'] = 'Nik_srekam';
$route['Check/Cetak'] = 'Nik_scetak';
$route['Check/ListDuplicate'] = 'Nik_list_duplicate';
$route['Check/Sfe'] = 'Nik_list_sfe';
$route['Check/Failure'] = 'Nik_list_failure';
$route['Check/Beginner'] = 'Nik_pemula';
$route['Check/FclPrr'] = 'Fcl_prr';
$route['Check/FclRekam'] = 'Fcl_srekam';
$route['Check/FclCetak'] = 'Fcl_scetak';
$route['Check/NotRecord'] = 'Nik_belum_rekam';


