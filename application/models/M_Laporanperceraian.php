<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class M_Laporanperceraian extends CI_Model {
		function __construct()
		{
			parent:: __construct();
		}

		public function get_data_perceraian($tgl,$tgl_start,$tgl_end,$no_kec,$no_kel){
			$sql = "SELECT 
						A.NO AS NO_WIL
						, UPPER(A.DESCRIP) AS NAMA_WIL
						, '$tgl' AS TANGGAL
						, CASE WHEN B.JUMLAH IS NULL THEN 0 ELSE B.JUMLAH END AS JUMLAH 
					FROM 
						REF_SIAK_WNI A
					LEFT JOIN 
						(
							SELECT 
								CERAI_SEBAB
								, COUNT(1) AS JUMLAH 
							FROM
								CAPIL_CERAI
							WHERE 
								1=1
								AND ADM_NO_PROV=32 
								AND ADM_NO_KAB=73 
								AND CERAI_TGL_LAPOR >= TO_DATE('".$tgl_start."','DD-MM-YYYY') 
								AND CERAI_TGL_LAPOR < TO_DATE('".$tgl_end."','DD-MM-YYYY')+1
               				GROUP BY 
               					CERAI_SEBAB
               			) B 
               		ON A.NO = B.CERAI_SEBAB 
					WHERE A.SECT =621
               		ORDER BY A.NO";
				$q = $this->db->query($sql);
               return $q->result();


		}

		public function get_jumlah_perceraian($tgl,$tgl_start,$tgl_end,$no_kec,$no_kel){
			$sql ="SELECT 
							COUNT(1) AS JUMLAH 
						FROM
							CAPIL_CERAI
						WHERE 
							1=1
							AND ADM_NO_PROV=32 
							AND ADM_NO_KAB=73 
							AND CERAI_TGL_LAPOR >= TO_DATE('".$tgl_start."','DD-MM-YYYY') 
							AND CERAI_TGL_LAPOR < TO_DATE('".$tgl_end."','DD-MM-YYYY')+1";	
				$q = $this->db->query($sql);
				$q = $this->db->query($sql);
               return $q->result();


		}
		function get_no_prop()
    	{
         $CI =& get_instance();
         $CI->load->model('M_Shared');
         return $CI->M_Shared->get_no_prop();
    	}
    	function get_no_kab()
    	{
         $CI =& get_instance();
         $CI->load->model('M_Shared');
         return $CI->M_Shared->get_no_kab();
    	}
    	function get_siak_dblink()
    	{
         $CI =& get_instance();
         $CI->load->model('M_Shared');
         return $CI->M_Shared->get_siak_dblink();
    	}
    	function get_rekam_dblink()
    	{
         $CI =& get_instance();
         $CI->load->model('M_Shared');
         return $CI->M_Shared->get_rekam_dblink();
    	}
    	function get_cetak_dblink()
    	{
         $CI =& get_instance();
         $CI->load->model('M_Shared');
         return $CI->M_Shared->get_cetak_dblink();
    	}
    	function get_master_dblink()
    	{
         $CI =& get_instance();
         $CI->load->model('M_Shared');
         return $CI->M_Shared->get_master_dblink();
    	}
}