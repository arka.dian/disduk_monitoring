<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class M_Shared extends CI_Model {
		function __construct()
		{
			parent:: __construct();
			$this->yzdb = $this->load->database('YZDB', TRUE);
		}
		
		public function get_all_kecamatan($no_kec = 0){
			$sql = "";
			$sql .= "SELECT NO_KEC, NAMA_KEC FROM SETUP_KEC@DB222 WHERE NO_PROP = 32 AND NO_KAB =  73 ";
			if ($no_kec != 0){
				$sql .= " AND NO_KEC = $no_kec ";
			}
			$sql .= " ORDER BY NO_KEC";
			$q = $this->yzdb->query($sql);
			return $q->result();
		}
		public function get_one_kecamatan($no_kec = 0){
			$sql ="";
			$sql .=" SELECT NO_KEC, NAMA_KEC FROM SETUP_KEC@DB222 WHERE NO_PROP = 32 AND NO_KAB =  73";
			if($no_kec != 0){
			$sql .=" AND NO_KEC = $no_kec";	
			}
			$sql .=" ORDER BY NO_KEC";
			$q = $this->yzdb->query($sql);
			return $q->result();
		}

		public function get_all_kelurahan($no_kec=0, $no_kel=0){
			$sql = "";
			$sql .= "SELECT NO_KEL, NAMA_KEL, NO_KEC FROM SETUP_KEL@DB222 WHERE NO_PROP= 32 AND NO_KAB= 73 ";
			if ($no_kec != 0){
				$sql .= " AND NO_KEC = $no_kec ";
			}
			if ($no_kel != 0){
				$sql .= " AND NO_KEL = $no_kel ";
			}
			  $sql .= " ORDER BY NO_KEC,NO_KEL";
			$q = $this->yzdb->query($sql);
			return $q->result();
		}
		public function get_all_rw($no_kec=0,$no_kel=0){
			$sql = "SELECT NO_KEC, NO_KEL, NO_RW, JML FROM (SELECT NO_KEC, NO_KEL,NO_RW, COUNT(1) JML FROM DATA_KELUARGA@DB222 WHERE NO_PROP= 32 AND NO_KAB= 73 AND NO_KEC = $no_kec AND NO_KEL = $no_kel AND NO_RW IS NOT NULL GROUP BY NO_KEC, NO_KEL, NO_RW ORDER BY NO_KEC, NO_KEL, NO_RW) WHERE JML > 50 ORDER BY NO_KEC, NO_KEL, NO_RW";
			$q = $this->yzdb->query($sql);
			return $q->result();
		}
		public function get_all_rt($no_kec=0,$no_kel=0,$no_rw=0){
			$sql = "SELECT NO_KEC, NO_KEL, NO_RW,NO_RT, JML FROM (SELECT NO_KEC, NO_KEL,NO_RW,NO_RT, COUNT(1) JML FROM DATA_KELUARGA@DB222 WHERE NO_PROP= 32 AND NO_KAB= 73 AND NO_KEC = $no_kec AND NO_KEL = $no_kel AND NO_RW = $no_rw AND NO_RW IS NOT NULL AND NO_RT IS NOT NULL GROUP BY NO_KEC, NO_KEL, NO_RW, NO_RT ORDER BY NO_KEC, NO_KEL, NO_RW, NO_RT) WHERE JML > 5 ORDER BY NO_KEC, NO_KEL, NO_RW, NO_RT";
			$q = $this->yzdb->query($sql);
			return $q->result();
		}
		public function get_nama_kec($no_kec){
			$sql = "SELECT NAMA_KEC FROM SETUP_KEC@DB222 WHERE NO_PROP= 32 AND NO_KAB= 73 AND NO_KEC = $no_kec";
			$q = $this->yzdb->query($sql);
			$r = $q->result();
			return $r[0]->NAMA_KEC;
		}
		public function get_nama_kel($no_kec, $no_kel){
			$sql = "SELECT NAMA_KEL FROM SETUP_KEL@DB222 WHERE NO_PROP= 32 AND NO_KAB= 73 AND NO_KEC = $no_kec AND NO_KEL = $no_kel";
			$q = $this->yzdb->query($sql);
			$r = $q->result();
			return $r[0]->NAMA_KEL;
		}
		public function get_all_wil(){
			$sql = "SELECT NO_WIL, NAMA_WIL FROM SIAK_KODE_WIL ORDER BY NO_WIL";
			$q = $this->yzdb->query($sql);
			return $q->result();
		}
		public function get_master_menu(){
			$sql = "SELECT MENU_ID, MENU_DESC FROM SIAK_MASTER_LAPORAN ORDER BY MENU_ID";
			$q = $this->yzdb->query($sql);
			return $q->result();
		}
		public function get_master_usia(){
			$sql = "SELECT MENU_ID, MENU_DESC FROM SIAK_MASTER_USIA ORDER BY MENU_ID";
			$q = $this->yzdb->query($sql);
			return $q->result();
		}
		public function get_master_cakupan(){
			$sql = "SELECT MENU_ID, MENU_DESC FROM SIAK_MASTER_CAKUPAN ORDER BY MENU_ID";
			$q = $this->yzdb->query($sql);
			return $q->result();
		}
		public function get_dkb(){
			$sql = "SELECT SYSTEM_VALUE_TXT AS VAL FROM SIAK_MASTER WHERE SYSTEM_TYPE = 'INIT_SETING' AND SYSTEM_CODE = 'DKB'";
			$q = $this->yzdb->query($sql);
			$r = $q->result();
			return $r[0]->VAL;
		}
		public function get_dkb_bio(){
			$sql = "SELECT SYSTEM_VALUE_TXT AS VAL FROM SIAK_MASTER WHERE SYSTEM_TYPE = 'INIT_SETING' AND SYSTEM_CODE = 'DKB_BIO'";
			$q = $this->yzdb->query($sql);
			$r = $q->result();
			return $r[0]->VAL;
		}
		public function get_dkb_keluarga(){
			$sql = "SELECT SYSTEM_VALUE_TXT AS VAL FROM SIAK_MASTER WHERE SYSTEM_TYPE = 'INIT_SETING' AND SYSTEM_CODE = 'DKB_KELUARGA'";
			$q = $this->yzdb->query($sql);
			$r = $q->result();
			return $r[0]->VAL;
		}
		public function get_cut_off_date(){
			$sql = "SELECT SYSTEM_VALUE_TXT AS VAL FROM SIAK_MASTER WHERE SYSTEM_TYPE = 'INIT_SETING' AND SYSTEM_CODE = 'DKB_CUTOFF'";
			$q = $this->yzdb->query($sql);
			$r = $q->result();
			return $r[0]->VAL;
		}
		
		public function get_user_daily(){
			$sql = "SELECT DISTINCT(USER_ID) FROM SIAK_DAILY ORDER BY USER_ID";
			$q = $this->yzdb->query($sql);
			return $q->result();
		}
		public function get_no_prop(){
			$sql = "SELECT SYSTEM_VALUE_NUM AS VAL FROM SIAK_MASTER WHERE SYSTEM_TYPE = 'INIT_SETING' AND SYSTEM_CODE = 'NO_PROP'";
			$q = $this->yzdb->query($sql);
			$r = $q->result();
			return $r[0]->VAL;
		}
		public function get_no_kab(){
			$sql = "SELECT SYSTEM_VALUE_NUM AS VAL FROM SIAK_MASTER WHERE SYSTEM_TYPE = 'INIT_SETING' AND SYSTEM_CODE = 'NO_KAB'";
			$q = $this->yzdb->query($sql);
			$r = $q->result();
			return $r[0]->VAL;
		}
		public function get_nm_prop(){
			$sql = "SELECT SYSTEM_VALUE_TXT AS VAL FROM SIAK_MASTER WHERE SYSTEM_TYPE = 'INIT_SETING' AND SYSTEM_CODE = 'NM_PROP'";
			$q = $this->yzdb->query($sql);
			$r = $q->result();
			return $r[0]->VAL;
		}
		public function get_nm_kab(){
			$sql = "SELECT SYSTEM_VALUE_TXT AS VAL FROM SIAK_MASTER WHERE SYSTEM_TYPE = 'INIT_SETING' AND SYSTEM_CODE = 'NM_KAB'";
			$q = $this->yzdb->query($sql);
			$r = $q->result();
			return $r[0]->VAL;
		}
		public function get_siak_dblink(){
			$sql = "SELECT SYSTEM_VALUE_TXT AS VAL FROM SIAK_MASTER WHERE SYSTEM_TYPE = 'INIT_SETING' AND SYSTEM_CODE = 'SIAK_DBLINK'";
			$q = $this->yzdb->query($sql);
			$r = $q->result();
			return $r[0]->VAL;
		}
		public function get_rekam_dblink(){
			$sql = "SELECT SYSTEM_VALUE_TXT AS VAL FROM SIAK_MASTER WHERE SYSTEM_TYPE = 'INIT_SETING' AND SYSTEM_CODE = 'REKAM_DBLINK'";
			$q = $this->yzdb->query($sql);
			$r = $q->result();
			return $r[0]->VAL;
		}
		public function get_cetak_dblink(){
			$sql = "SELECT SYSTEM_VALUE_TXT AS VAL FROM SIAK_MASTER WHERE SYSTEM_TYPE = 'INIT_SETING' AND SYSTEM_CODE = 'CETAK_DBLINK'";
			$q = $this->yzdb->query($sql);
			$r = $q->result();
			return $r[0]->VAL;
		}
		public function get_master_dblink(){
			$sql = "SELECT SYSTEM_VALUE_TXT AS VAL FROM SIAK_MASTER WHERE SYSTEM_TYPE = 'INIT_SETING' AND SYSTEM_CODE = 'MASTER_DBLINK'";
			$q = $this->yzdb->query($sql);
			$r = $q->result();
			return $r[0]->VAL;
		}
		public function is_valid(){
			$sql = "SELECT SYSTEM_VALUE_TXT AS VAL FROM SIAK_MASTER WHERE SYSTEM_TYPE = 'INIT_SETING' AND SYSTEM_CODE = 'EXP'";
			$q = $this->yzdb->query($sql);
			$r = $q->result();
			return $r[0]->VAL;
		}
		public function get_menu($level = 0){
			$sql = "SELECT A.MENU_ID,A.PARENT_ID, A.ICON,A.TITLE,A.URL,A.IS_ACTIVE, B.USER_LEVEL, A.MENU_LEVEL FROM SIAK_MENU A INNER JOIN SIAK_AKSES B ON A.MENU_ID = B.MENU_ID WHERE B.USER_LEVEL = $level AND A.MENU_LEVEL = 1 ORDER BY A.PARENT_ID,A.MENU_ID";
			$q = $this->yzdb->query($sql);
			$r = $q->result();
			$i=0;
        	foreach($r as $row){
            	$r[$i]->SUB_MENU = $this->get_sub_menu($row->MENU_ID,$level);
            	$i++;
        	}
        	return $r;
		}
		public function get_sub_menu($parent_id = 0,$level = 0){
			$sql = "SELECT A.MENU_ID,A.PARENT_ID, A.ICON,A.TITLE,A.URL,A.IS_ACTIVE, B.USER_LEVEL, A.MENU_LEVEL FROM SIAK_MENU A INNER JOIN SIAK_AKSES B ON A.MENU_ID = B.MENU_ID WHERE B.USER_LEVEL = $level AND A.PARENT_ID = $parent_id ORDER BY A.PARENT_ID,A.MENU_ID";
			$q = $this->yzdb->query($sql);
			$r = $q->result();
			$i=0;
        	foreach($r as $row){
        		if($row->IS_ACTIVE != 1){
            	$r[$i]->SUB_MENU = $this->get_sub_menu($row->MENU_ID,$level);
            	}
            	$i++;
        	}
        	return $r;
		}

		public function get_islogin($ip_address,$user_id){
			$sql = "SELECT COUNT(1) VAL FROM SIAK_SESSION_PLUS WHERE IP_ADDRESS = '$ip_address' AND USER_ID = '$user_id' AND IS_ACTIVE = 1";
			$q = $this->yzdb->query($sql);
			$r = $q->result();
			return $r[0]->VAL;
		}
		 public function stop_activity($user_id){
        	$sql = "SELECT COUNT(1) AS CNT FROM SIAK_SESSION_PLUS WHERE USER_ID = '$user_id'";
			$q = $this->yzdb->query($sql);
             $r = (int) $q->row()->CNT;
             if($r > 0){
                $sql = "UPDATE SIAK_SESSION_PLUS SET LAST_ACTIVITY = SYSDATE, IS_ACTIVE = 0  WHERE USER_ID = '$user_id'";
                $this->yzdb->query($sql);
             }
             
        }


}