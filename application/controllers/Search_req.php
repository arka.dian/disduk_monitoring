<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Search_req extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->model('M_Search','scr');	
		$this->load->model('M_Shared','shr');	
		if ($this->session->userdata(S_SESSION_ID) == null) 
		{
			redirect('/','refresh');
		} else {
			$is_log = $this->shr->get_islogin($this->session->userdata(S_IP_ADDRESS),$this->session->userdata(S_USER_ID));
			if ($is_log == 0){
				if ($this->session->userdata(S_SESSION_ID) != null) {
		 		$this->shr->stop_activity($this->session->userdata(S_USER_ID));
		 		}
				$this->session->sess_destroy();
			}
		}
	}
	public function index()
	{
           redirect('/','refresh');
	}

	public function uktp() 
	{
			$menu = $this->shr->get_menu($this->session->userdata(S_USER_LEVEL));
			if($this->input->post('no_kec') != null){
			$tgl = $this->input->post('tanggal');
			$tgl_start = substr($tgl, 0, 10);
			$tgl_end = substr($tgl,13, 10);
			$no_kec = $this->input->post('no_kec');
			$no_kel = $this->input->post('no_kel');
			$nik = $this->input->post('nik');
			$r = $this->scr->cek_req_data_uktp($nik,$tgl_start,$tgl_end,$no_kec,$no_kel);
			// $j = $this->scr->cek_req_count_uktp($nik,$tgl_start,$tgl_end,$no_kec,$no_kel);
			$data = array(
		 		"stitle"=>'Request Cetak Ktp',
		 		"mtitle"=>'Request Cetak Ktp (Belum Cetak Without Filter)',
		 		"my_url"=>'uktp',
		 		"type_tgl"=>'Request',
		 		"data"=>$r,
		 		// "jumlah"=>$j,
		 		"menu"=>$menu,
		 		"user_id"=>$this->session->userdata(S_USER_ID),
		 		"user_nik"=>$this->session->userdata(S_NIK),
		 		"user_nama_lgkp"=>$this->session->userdata(S_NAMA_LGKP),
		 		"user_nama_dpn"=>$this->session->userdata(S_NAMA_DPN),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL),
		 		"user_no_kec"=>$this->session->userdata(S_NO_KEC),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL)
    		);
			}else{
			$data = array(
		 		"stitle"=>'Request Cetak Ktp',
		 		"mtitle"=>'Request Cetak Ktp (Belum Cetak Without Filter)',
		 		"my_url"=>'uktp',
		 		"type_tgl"=>'Request',
		 		"menu"=>$menu,
		 		"user_id"=>$this->session->userdata(S_USER_ID),
		 		"user_nik"=>$this->session->userdata(S_NIK),
		 		"user_nama_lgkp"=>$this->session->userdata(S_NAMA_LGKP),
		 		"user_nama_dpn"=>$this->session->userdata(S_NAMA_DPN),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL),
		 		"user_no_kec"=>$this->session->userdata(S_NO_KEC),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL)
    		);
    		}
			$this->load->view('Cekreq/index',$data);
	}

	public function dktp() 
	{
			$menu = $this->shr->get_menu($this->session->userdata(S_USER_LEVEL));
			if($this->input->post('no_kec') != null){
			$tgl = $this->input->post('tanggal');
			$tgl_start = substr($tgl, 0, 10);
			$tgl_end = substr($tgl,13, 10);
			$no_kec = $this->input->post('no_kec');
			$no_kel = $this->input->post('no_kel');
			$nik = $this->input->post('nik');
			$r = $this->scr->cek_req_data_dktp($nik,$tgl_start,$tgl_end,$no_kec,$no_kel);
			// $j = $this->scr->cek_req_count_dktp($nik,$tgl_start,$tgl_end,$no_kec,$no_kel);
			$data = array(
		 		"stitle"=>'Request Cetak Ktp',
		 		"mtitle"=>'Request Cetak Ktp (Sudah Cetak Without Filter)',
		 		"my_url"=>'dktp',
		 		"type_tgl"=>'Request',
		 		"data"=>$r,
		 		// "jumlah"=>$j,
		 		"menu"=>$menu,
		 		"user_id"=>$this->session->userdata(S_USER_ID),
		 		"user_nik"=>$this->session->userdata(S_NIK),
		 		"user_nama_lgkp"=>$this->session->userdata(S_NAMA_LGKP),
		 		"user_nama_dpn"=>$this->session->userdata(S_NAMA_DPN),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL),
		 		"user_no_kec"=>$this->session->userdata(S_NO_KEC)
    		);
			}else{
			$data = array(
		 		"stitle"=>'Request Cetak Ktp',
		 		"mtitle"=>'Request Cetak Ktp (Sudah Cetak Without Filter)',
		 		"my_url"=>'dktp',
		 		"type_tgl"=>'Request',
		 		"menu"=>$menu,
		 		"user_id"=>$this->session->userdata(S_USER_ID),
		 		"user_nik"=>$this->session->userdata(S_NIK),
		 		"user_nama_lgkp"=>$this->session->userdata(S_NAMA_LGKP),
		 		"user_nama_dpn"=>$this->session->userdata(S_NAMA_DPN),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL),
		 		"user_no_kec"=>$this->session->userdata(S_NO_KEC)
    		);
    		}
			$this->load->view('Cekcetak/index',$data);
	}
	public function uktp_f() 
	{
			$menu = $this->shr->get_menu($this->session->userdata(S_USER_LEVEL));
			if($this->input->post('no_kec') != null){
			$tgl = $this->input->post('tanggal');
			$tgl_start = substr($tgl, 0, 10);
			$tgl_end = substr($tgl,13, 10);
			$no_kec = $this->input->post('no_kec');
			$no_kel = $this->input->post('no_kel');
			$nik = $this->input->post('nik');
			$r = $this->scr->cek_req_data_uktp_f($nik,$tgl_start,$tgl_end,$no_kec,$no_kel);
			// $j = $this->scr->cek_req_count_uktp_f($nik,$tgl_start,$tgl_end,$no_kec,$no_kel);
			$data = array(
		 		"stitle"=>'Request Cetak Ktp',
		 		"mtitle"=>'Request Cetak Ktp (Belum Cetak With Filter)',
		 		"my_url"=>'uktp_f',
		 		"type_tgl"=>'Request',
		 		"data"=>$r,
		 		// "jumlah"=>$j,
		 		"menu"=>$menu,
		 		"user_id"=>$this->session->userdata(S_USER_ID),
		 		"user_nik"=>$this->session->userdata(S_NIK),
		 		"user_nama_lgkp"=>$this->session->userdata(S_NAMA_LGKP),
		 		"user_nama_dpn"=>$this->session->userdata(S_NAMA_DPN),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL),
		 		"user_no_kec"=>$this->session->userdata(S_NO_KEC),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL)
    		);
			}else{
			$data = array(
		 		"stitle"=>'Request Cetak Ktp',
		 		"mtitle"=>'Request Cetak Ktp (Belum Cetak With Filter)',
		 		"my_url"=>'uktp_f',
		 		"type_tgl"=>'Request',
		 		"menu"=>$menu,
		 		"user_id"=>$this->session->userdata(S_USER_ID),
		 		"user_nik"=>$this->session->userdata(S_NIK),
		 		"user_nama_lgkp"=>$this->session->userdata(S_NAMA_LGKP),
		 		"user_nama_dpn"=>$this->session->userdata(S_NAMA_DPN),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL),
		 		"user_no_kec"=>$this->session->userdata(S_NO_KEC),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL)
    		);
    		}
			$this->load->view('Cekreq/index',$data);
	}

	public function dktp_f() 
	{
			$menu = $this->shr->get_menu($this->session->userdata(S_USER_LEVEL));
			if($this->input->post('no_kec') != null){
			$tgl = $this->input->post('tanggal');
			$tgl_start = substr($tgl, 0, 10);
			$tgl_end = substr($tgl,13, 10);
			$no_kec = $this->input->post('no_kec');
			$no_kel = $this->input->post('no_kel');
			$nik = $this->input->post('nik');
			$r = $this->scr->cek_req_data_dktp_f($nik,$tgl_start,$tgl_end,$no_kec,$no_kel);
			// $j = $this->scr->cek_req_count_dktp_f($nik,$tgl_start,$tgl_end,$no_kec,$no_kel);
			$data = array(
		 		"stitle"=>'Request Cetak Ktp',
		 		"mtitle"=>'Request Cetak Ktp (Sudah Cetak With Filter)',
		 		"my_url"=>'dktp_f',
		 		"type_tgl"=>'Request',
		 		"data"=>$r,
		 		// "jumlah"=>$j,
		 		"menu"=>$menu,
		 		"user_id"=>$this->session->userdata(S_USER_ID),
		 		"user_nik"=>$this->session->userdata(S_NIK),
		 		"user_nama_lgkp"=>$this->session->userdata(S_NAMA_LGKP),
		 		"user_nama_dpn"=>$this->session->userdata(S_NAMA_DPN),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL),
		 		"user_no_kec"=>$this->session->userdata(S_NO_KEC)
    		);
			}else{
			$data = array(
		 		"stitle"=>'Request Cetak Ktp',
		 		"mtitle"=>'Request Cetak Ktp (Sudah Cetak With Filter)',
		 		"my_url"=>'dktp_f',
		 		"type_tgl"=>'Request',
		 		"menu"=>$menu,
		 		"user_id"=>$this->session->userdata(S_USER_ID),
		 		"user_nik"=>$this->session->userdata(S_NIK),
		 		"user_nama_lgkp"=>$this->session->userdata(S_NAMA_LGKP),
		 		"user_nama_dpn"=>$this->session->userdata(S_NAMA_DPN),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL),
		 		"user_no_kec"=>$this->session->userdata(S_NO_KEC)
    		);
    		}
			$this->load->view('Cekcetak/index',$data);
	}

	public function usuket() 
	{
			$menu = $this->shr->get_menu($this->session->userdata(S_USER_LEVEL));
			if($this->input->post('no_kec') != null){
			$tgl = $this->input->post('tanggal');
			$tgl_start = substr($tgl, 0, 10);
			$tgl_end = substr($tgl,13, 10);
			$no_kec = $this->input->post('no_kec');
			$no_kel = $this->input->post('no_kel');
			$nik = $this->input->post('nik');
			$r = $this->scr->cek_req_data_usuket($nik,$tgl_start,$tgl_end,$no_kec,$no_kel);
			// $j = $this->scr->cek_req_count_usuket($nik,$tgl_start,$tgl_end,$no_kec,$no_kel);
			$data = array(
		 		"stitle"=>'Request Cetak Suket',
		 		"mtitle"=>'Request Cetak Suket (Belum Cetak)',
		 		"my_url"=>'usuket',
		 		"type_tgl"=>'Request',
		 		"data"=>$r,
		 		// "jumlah"=>$j,
		 		"menu"=>$menu,
		 		"user_id"=>$this->session->userdata(S_USER_ID),
		 		"user_nik"=>$this->session->userdata(S_NIK),
		 		"user_nama_lgkp"=>$this->session->userdata(S_NAMA_LGKP),
		 		"user_nama_dpn"=>$this->session->userdata(S_NAMA_DPN),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL),
		 		"user_no_kec"=>$this->session->userdata(S_NO_KEC)
    		);
			}else{
			$data = array(
		 		"stitle"=>'Request Cetak Suket',
		 		"mtitle"=>'Request Cetak Suket (Belum Cetak)',
		 		"my_url"=>'usuket',
		 		"type_tgl"=>'Request',
		 		"menu"=>$menu,
		 		"user_id"=>$this->session->userdata(S_USER_ID),
		 		"user_nik"=>$this->session->userdata(S_NIK),
		 		"user_nama_lgkp"=>$this->session->userdata(S_NAMA_LGKP),
		 		"user_nama_dpn"=>$this->session->userdata(S_NAMA_DPN),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL),
		 		"user_no_kec"=>$this->session->userdata(S_NO_KEC)
    		);
    		}
			$this->load->view('Cekreq/index',$data);
	}

	public function dsuket() 
	{
			$menu = $this->shr->get_menu($this->session->userdata(S_USER_LEVEL));
			if($this->input->post('no_kec') != null){
			$tgl = $this->input->post('tanggal');
			$tgl_start = substr($tgl, 0, 10);
			$tgl_end = substr($tgl,13, 10);
			$no_kec = $this->input->post('no_kec');
			$no_kel = $this->input->post('no_kel');
			$nik = $this->input->post('nik');
			$r = $this->scr->cek_req_data_dsuket($nik,$tgl_start,$tgl_end,$no_kec,$no_kel);
			// $j = $this->scr->cek_req_count_dsuket($nik,$tgl_start,$tgl_end,$no_kec,$no_kel);
			$data = array(
		 		"stitle"=>'Request Cetak Suket',
		 		"mtitle"=>'Request Cetak Suket (Sudah Cetak)',
		 		"my_url"=>'dsuket',
		 		"type_tgl"=>'Request',
		 		"data"=>$r,
		 		// "jumlah"=>$j,
		 		"menu"=>$menu,
		 		"user_id"=>$this->session->userdata(S_USER_ID),
		 		"user_nik"=>$this->session->userdata(S_NIK),
		 		"user_nama_lgkp"=>$this->session->userdata(S_NAMA_LGKP),
		 		"user_nama_dpn"=>$this->session->userdata(S_NAMA_DPN),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL),
		 		"user_no_kec"=>$this->session->userdata(S_NO_KEC)
    		);
			}else{
			$data = array(
		 		"stitle"=>'Request Cetak Suket',
		 		"mtitle"=>'Request Cetak Suket (Sudah Cetak)',
		 		"my_url"=>'dsuket',
		 		"type_tgl"=>'Request',
		 		"menu"=>$menu,
		 		"user_id"=>$this->session->userdata(S_USER_ID),
		 		"user_nik"=>$this->session->userdata(S_NIK),
		 		"user_nama_lgkp"=>$this->session->userdata(S_NAMA_LGKP),
		 		"user_nama_dpn"=>$this->session->userdata(S_NAMA_DPN),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL),
		 		"user_no_kec"=>$this->session->userdata(S_NO_KEC)
    		);
    		}
			$this->load->view('Cekcetak/index',$data);
	}
	public function req_bio() 
	{
			$menu = $this->shr->get_menu($this->session->userdata(S_USER_LEVEL));
			if($this->input->post('no_kec') != null){
			$tgl = $this->input->post('tanggal');
			$tgl_start = substr($tgl, 0, 10);
			$tgl_end = substr($tgl,13, 10);
			$no_kec = $this->input->post('no_kec');
			$no_kel = $this->input->post('no_kel');
			$nik = $this->input->post('nik');
			$r = $this->scr->cek_req_data_bio($nik,$tgl_start,$tgl_end,$no_kec,$no_kel);
			// $j = $this->scr->cek_req_count_bio($nik,$tgl_start,$tgl_end,$no_kec,$no_kel);
			$data = array(
		 		"stitle"=>'Request Biometrik Kosong',
		 		"mtitle"=>'Request Biometrik Kosong',
		 		"my_url"=>'req_bio',
		 		"type_tgl"=>'Request',
		 		"data"=>$r,
		 		// "jumlah"=>$j,
		 		"menu"=>$menu,
		 		"user_id"=>$this->session->userdata(S_USER_ID),
		 		"user_nik"=>$this->session->userdata(S_NIK),
		 		"user_nama_lgkp"=>$this->session->userdata(S_NAMA_LGKP),
		 		"user_nama_dpn"=>$this->session->userdata(S_NAMA_DPN),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL),
		 		"user_no_kec"=>$this->session->userdata(S_NO_KEC)
    		);
			}else{
			$data = array(
		 		"stitle"=>'Request Biometrik Kosong',
		 		"mtitle"=>'Request Biometrik Kosong',
		 		"my_url"=>'req_bio',
		 		"type_tgl"=>'Request',
		 		"menu"=>$menu,
		 		"user_id"=>$this->session->userdata(S_USER_ID),
		 		"user_nik"=>$this->session->userdata(S_NIK),
		 		"user_nama_lgkp"=>$this->session->userdata(S_NAMA_LGKP),
		 		"user_nama_dpn"=>$this->session->userdata(S_NAMA_DPN),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL),
		 		"user_no_kec"=>$this->session->userdata(S_NO_KEC)
    		);
    		}
			$this->load->view('Reqbio/index',$data);
	}
	public function delete_req() 
	{
			$menu = $this->shr->get_menu($this->session->userdata(S_USER_LEVEL));
			if($this->input->post('nik') != null){
			$nik = $this->input->post('nik');
			if (substr($nik, 0, 1) === ','){
				$nik = ltrim($nik, ',');
			}
			$r = $this->scr->del_req($nik,$this->session->userdata(S_NO_KEC));
			// $j = $this->scr->cek_req_count_bio($nik,$tgl_start,$tgl_end,$no_kec,$no_kel);
			$data = array(
		 		"stitle"=>'Delete Pengajuan Bermasalah',
		 		"mtitle"=>'Delete Pengajuan Bermasalah',
		 		"my_url"=>'delete_req',
		 		"type_tgl"=>'Pengajuan',
		 		"data"=>$r,
		 		// "jumlah"=>$j,
		 		"menu"=>$menu,
		 		"user_id"=>$this->session->userdata(S_USER_ID),
		 		"user_nik"=>$this->session->userdata(S_NIK),
		 		"user_nama_lgkp"=>$this->session->userdata(S_NAMA_LGKP),
		 		"user_nama_dpn"=>$this->session->userdata(S_NAMA_DPN),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL),
		 		"user_no_kec"=>$this->session->userdata(S_NO_KEC)
    		);
			}else{
			$data = array(
		 		"stitle"=>'Delete Pengajuan Bermasalah',
		 		"mtitle"=>'Delete Pengajuan Bermasalah',
		 		"my_url"=>'delete_req',
		 		"type_tgl"=>'Pengajuan',
		 		"menu"=>$menu,
		 		"user_id"=>$this->session->userdata(S_USER_ID),
		 		"user_nik"=>$this->session->userdata(S_NIK),
		 		"user_nama_lgkp"=>$this->session->userdata(S_NAMA_LGKP),
		 		"user_nama_dpn"=>$this->session->userdata(S_NAMA_DPN),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL),
		 		"user_no_kec"=>$this->session->userdata(S_NO_KEC)
    		);
    		}
			$this->load->view('Deletereq/index',$data);
	}
	public function do_delete() 
	{
			$menu = $this->shr->get_menu($this->session->userdata(S_USER_LEVEL));
			if($this->input->post('niks') != null){
			$nik = $this->input->post('niks');
			if (substr($nik, 0, 1) === ','){
				$nik = ltrim($nik, ',');
			}
				$this->scr->do_delete($nik);
				redirect('Search_req/delete_req','refresh');
			}else{
				redirect('Search_req/delete_req','refresh');	
    		}    		
	}
}