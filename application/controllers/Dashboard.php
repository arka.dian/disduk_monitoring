<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->model('M_Android','andro');
		$this->load->model('M_Shared','shr');
		$this->load->model('M_Dashboard','dsb');
		if ($this->session->userdata(S_SESSION_ID) == null) 
		{
			redirect('/','refresh');
		} else {
			$is_log = $this->shr->get_islogin($this->session->userdata(S_IP_ADDRESS),$this->session->userdata(S_USER_ID));
			if ($is_log == 0){
				if ($this->session->userdata(S_SESSION_ID) != null) {
		 		$this->shr->stop_activity($this->session->userdata(S_USER_ID));
		 		}
				$this->session->sess_destroy();
			}
		}
	}
	public function index()
	{
		
           redirect('/','refresh');
        
		
	}
	public function demogh()
	{
		
			$menu = $this->shr->get_menu($this->session->userdata(S_USER_LEVEL));
			$data = array(
		 		"stitle"=>'Dashboard',
		 		"menu"=>$menu,
		 		"user_id"=>$this->session->userdata(S_USER_ID),
		 		"user_nik"=>$this->session->userdata(S_NIK),
		 		"user_nama_lgkp"=>$this->session->userdata(S_NAMA_LGKP),
		 		"user_nama_dpn"=>$this->session->userdata(S_NAMA_DPN),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL)
    		);
			$this->load->view('Dashboard_demogh/index',$data);
				
	}
	public function ktpel()
	{
		
			$menu = $this->shr->get_menu($this->session->userdata(S_USER_LEVEL));
			$r = $this->dsb->get_user_cetak();
			$data = array(
		 		"stitle"=>'Dashboard',
		 		"menu"=>$menu,
		 		"user_cetak"=>$r,
		 		"user_id"=>$this->session->userdata(S_USER_ID),
		 		"user_nik"=>$this->session->userdata(S_NIK),
		 		"user_nama_lgkp"=>$this->session->userdata(S_NAMA_LGKP),
		 		"user_nama_dpn"=>$this->session->userdata(S_NAMA_DPN),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL)
    		);
			$this->load->view('dashboard_cetak_ktpel/index',$data);
				
	}
	public function get_dashboard_m1()
	{
		header('Content-type: application/json');
		$get_dashboard = $this->andro->get_dashboard_m1();	
		$response["perekaman_today"] = $get_dashboard[0]->JML;
        echo json_encode($response);
	}
	public function get_dashboard_m2()
	{
		header('Content-type: application/json');
		$get_dashboard = $this->andro->get_dashboard_m2();	
		$response["pencetakan_today"] = $get_dashboard[0]->JML;
        echo json_encode($response);
	}
	public function get_dashboard_m3()
	{
		header('Content-type: application/json');
		$get_dashboard = $this->andro->get_dashboard_m3();	
		$response["sisa_prr"] = $get_dashboard[0]->JML;
        echo json_encode($response);
	}
	public function get_dashboard_m4()
	{
		header('Content-type: application/json');
		$get_dashboard = $this->andro->get_dashboard_m4();	
		$response["sisa_sfe"] = $get_dashboard[0]->JML;
        echo json_encode($response);
	}
	public function get_dashboard_m5()
	{
		header('Content-type: application/json');
		$get_dashboard = $this->andro->get_dashboard_m5();	
		$response["sisa_suket"] = $get_dashboard[0]->JML;
        echo json_encode($response);
	}
	public function get_dashboard_m6()
	{
		header('Content-type: application/json');
		$get_dashboard = $this->andro->get_dashboard_m6();	
		$response["blangko_out"] = $get_dashboard[0]->JML;
        echo json_encode($response);
	}
	public function get_dashboard_m7()
	{
		header('Content-type: application/json');
		$get_dashboard = $this->andro->get_dashboard_m7();	
		$response["duplicate"] = $get_dashboard[0]->JML;
        echo json_encode($response);
	}
	public function get_dashboard_m8()
	{
		header('Content-type: application/json');
		$get_dashboard = $this->andro->get_dashboard_m8();	
		$response["sisa_blangko"] = $get_dashboard[0]->JML;
        echo json_encode($response);
	}
	public function get_dashboard_kk()
	{
		header('Content-type: application/json');
		$get_dashboard = $this->andro->get_dashboard_kk();	
		$response["pen_kk"] = $get_dashboard[0]->JML;
        echo json_encode($response);
	}
	public function get_dashboard_kia()
	{
		header('Content-type: application/json');
		$get_dashboard = $this->andro->get_dashboard_kia();	
		$response["pen_kia"] = $get_dashboard[0]->JML;
        echo json_encode($response);
	}
	public function get_dashboard_nik()
	{
		header('Content-type: application/json');
		$get_dashboard = $this->andro->get_dashboard_nik();	
		$response["pen_nik_baru"] = $get_dashboard[0]->JML;
        echo json_encode($response);
	}
	public function get_dashboard_akta_lu()
	{
		header('Content-type: application/json');
		$get_dashboard = $this->andro->get_dashboard_akta_lu();	
		$response["pen_lahir_lu"] = $get_dashboard[0]->JML;
        echo json_encode($response);
	}
	public function get_dashboard_akta_lt()
	{
		header('Content-type: application/json');
		$get_dashboard = $this->andro->get_dashboard_akta_lt();	
		$response["pen_lahir_lt"] = $get_dashboard[0]->JML;
        echo json_encode($response);
	}
	public function get_dashboard_akta_mt()
	{
		header('Content-type: application/json');
		$get_dashboard = $this->andro->get_dashboard_akta_mt();	
		$response["pen_mati"] = $get_dashboard[0]->JML;
        echo json_encode($response);
	}
	public function get_dashboard_akta_kwn()
	{
		header('Content-type: application/json');
		$get_dashboard = $this->andro->get_dashboard_akta_kwn();	
		$response["pen_kawin"] = $get_dashboard[0]->JML;
        echo json_encode($response);
	}
	public function get_dashboard_akta_cry()
	{
		header('Content-type: application/json');
		$get_dashboard = $this->andro->get_dashboard_akta_cry();	
		$response["pen_cerai"] = $get_dashboard[0]->JML;
        echo json_encode($response);
	}
	public function get_dashboard_pdh_akab()
	{
		header('Content-type: application/json');
		$get_dashboard = $this->andro->get_dashboard_pdh_akab();	
		$response["pen_pindah_akab"] = $get_dashboard[0]->JML;
        echo json_encode($response);
	}
	public function get_dashboard_pdh_akec()
	{
		header('Content-type: application/json');
		$get_dashboard = $this->andro->get_dashboard_pdh_akec();	
		$response["pen_pindah_akec"] = $get_dashboard[0]->JML;
        echo json_encode($response);
	}
	public function get_dashboard_pdh_dkec()
	{
		header('Content-type: application/json');
		$get_dashboard = $this->andro->get_dashboard_pdh_dkec();	
		$response["pen_pindah_dkec"] = $get_dashboard[0]->JML;
        echo json_encode($response);
	}
	public function get_dashboard_dtg_akab()
	{
		header('Content-type: application/json');
		$get_dashboard = $this->andro->get_dashboard_dtg_akab();	
		$response["pen_datang_akab"] = $get_dashboard[0]->JML;
        echo json_encode($response);
	}
	public function get_dashboard_dtg_akec()
	{
		header('Content-type: application/json');
		$get_dashboard = $this->andro->get_dashboard_dtg_akec();	
		$response["pen_datang_akec"] = $get_dashboard[0]->JML;
        echo json_encode($response);
	}
	public function get_dashboard_dtg_dkec()
	{
		header('Content-type: application/json');
		$get_dashboard = $this->andro->get_dashboard_dtg_dkec();	
		$response["pen_datang_dkec"] = $get_dashboard[0]->JML;
        echo json_encode($response);
	}
	public function get_dashboard_yesterday()
	{
		header('Content-type: application/json');
		$get_dashboard_yesterday = $this->andro->dashboard_yesterday();	
		// print_r($get_dashboard_yesterday);
		// die;
		$response["perekaman_yesterday"] = $get_dashboard_yesterday[0]->JML;
		$response["pencetakan_yesterday"] = $get_dashboard_yesterday[1]->JML;
		$response["sisa_prr_yesterday"] = $get_dashboard_yesterday[2]->JML;
		$response["sisa_sfe_yesterday"] = $get_dashboard_yesterday[3]->JML;
		$response["sisa_suket_yesterday"] = $get_dashboard_yesterday[4]->JML;
		$response["sisa_blangko_yesterday"] = $get_dashboard_yesterday[5]->JML;
		$response["duplicate_yesterday"] = $get_dashboard_yesterday[6]->JML;
		$response["failure_yesterday"] = $get_dashboard_yesterday[7]->JML;
		$response["blangko_out_yesterday"] = $get_dashboard_yesterday[8]->JML;
        echo json_encode($response);
	}
}