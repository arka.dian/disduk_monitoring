<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard_ktpel extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->model('M_Dashboard','dsb');
		$this->load->model('M_Shared','shr');
		if ($this->session->userdata(S_SESSION_ID) == null) 
		{
			redirect('/','refresh');
		} else {
			$is_log = $this->shr->get_islogin($this->session->userdata(S_IP_ADDRESS),$this->session->userdata(S_USER_ID));
			if ($is_log == 0){
				if ($this->session->userdata(S_SESSION_ID) != null) {
		 		$this->shr->stop_activity($this->session->userdata(S_USER_ID));
		 		}
				$this->session->sess_destroy();
			}
		}	
	}
	public function index()
	{
			$menu = $this->shr->get_menu($this->session->userdata(S_USER_LEVEL));
			$data = array(
		 		"stitle"=>'Dashboard Ktp-El',
		 		"back_title"=>'Dashboard',
		 		"backurl"=>'Dashboard',
		 		"menu"=>$menu,
		 		"user_id"=>$this->session->userdata(S_USER_ID),
		 		"user_nik"=>$this->session->userdata(S_NIK),
		 		"user_nama_lgkp"=>$this->session->userdata(S_NAMA_LGKP),
		 		"user_nama_dpn"=>$this->session->userdata(S_NAMA_DPN),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL)
    		);
			$this->load->view('Dashboard_ktpel/index',$data);
		
	}
	public function get_data_rkm()
	{
		header('Content-type: application/json');
		 $rkm = $this->dsb->get_rekam();
		 $output = $rkm;
        echo json_encode($output);
	}
	public function get_data_ctk()
	{
		header('Content-type: application/json');
		 $ctk = $this->dsb->get_cetak();
		 $output = $ctk;
        echo json_encode($output);
	}
	public function get_data_suket()
	{
		header('Content-type: application/json');
		 $skt = $this->dsb->get_suket();
		 $output = $skt;
        echo json_encode($output);
	}
	public function get_data_kia()
	{
		header('Content-type: application/json');
		 $kia = $this->dsb->get_kia();
		 $output = $kia;
        echo json_encode($output);
	}
}
