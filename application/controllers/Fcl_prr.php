<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Fcl_prr extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->model('M_Prr','prr');	
		$this->load->model('M_Shared','shr');
		if ($this->session->userdata(S_SESSION_ID) == null) 
		{
			redirect('/','refresh');
		} else {
			$is_log = $this->shr->get_islogin($this->session->userdata(S_IP_ADDRESS),$this->session->userdata(S_USER_ID));
			if ($is_log == 0){
				if ($this->session->userdata(S_SESSION_ID) != null) {
		 		$this->shr->stop_activity($this->session->userdata(S_USER_ID));
		 		}
				$this->session->sess_destroy();
			}
		}	
	}
	public function index()
	{
			$menu = $this->shr->get_menu($this->session->userdata(S_USER_LEVEL));
			if($this->input->post('no_kec') != null){
			$no_kec = $this->input->post('no_kec');
			$no_kel = 0;
			$r = $this->prr->get_list_prr_fcl($no_kec,$no_kel);
			$j = $this->prr->count_list_prr_fcl($no_kec,$no_kel);
			$data = array(
		 		"stitle"=>'List PRR Citilink',
		 		"mtitle"=>'List PRR Citilink',
		 		"my_url"=>'FclPrr',
		 		"type_tgl"=>'Tanggal Rekam',
		 		"data"=>$r,
		 		"jumlah"=>$j,
		 		"menu"=>$menu,
		 		"user_id"=>$this->session->userdata(S_USER_ID),
		 		"user_nik"=>$this->session->userdata(S_NIK),
		 		"user_nama_lgkp"=>$this->session->userdata(S_NAMA_LGKP),
		 		"user_nama_dpn"=>$this->session->userdata(S_NAMA_DPN),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL),
		 		"user_no_kec"=>$this->session->userdata(S_NO_KEC),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL)
    		);
			}else{
			$data = array(
		 		"stitle"=>'List PRR Citilink',
		 		"mtitle"=>'List PRR Citilink',
		 		"my_url"=>'FclPrr',
		 		"type_tgl"=>'Tanggal Rekam',
		 		"menu"=>$menu,
		 		"user_id"=>$this->session->userdata(S_USER_ID),
		 		"user_nik"=>$this->session->userdata(S_NIK),
		 		"user_nama_lgkp"=>$this->session->userdata(S_NAMA_LGKP),
		 		"user_nama_dpn"=>$this->session->userdata(S_NAMA_DPN),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL),
		 		"user_no_kec"=>$this->session->userdata(S_NO_KEC),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL)
    		);
    		}
			$this->load->view('Fclprr/index',$data);
				
	}
}