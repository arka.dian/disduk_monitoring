<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard_capil extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->model('M_Dashboard','dsb');
		$this->load->model('M_Shared','shr');
		if ($this->session->userdata(S_SESSION_ID) == null) 
		{
			redirect('/','refresh');
		} else {
			$is_log = $this->shr->get_islogin($this->session->userdata(S_IP_ADDRESS),$this->session->userdata(S_USER_ID));
			if ($is_log == 0){
				if ($this->session->userdata(S_SESSION_ID) != null) {
		 		$this->shr->stop_activity($this->session->userdata(S_USER_ID));
		 		}
				$this->session->sess_destroy();
			}
		}
		if ($this->session->userdata(S_USER_LEVEL) < 2) 
		{
			redirect('/','refresh');
		}
	}
	public function index()
	{
			$menu = $this->shr->get_menu($this->session->userdata(S_USER_LEVEL));
			$data = array(
		 		"stitle"=>'Dashboard Capil',
		 		"back_title"=>'Dashboard',
		 		"backurl"=>'Dashboard',
		 		"menu"=>$menu,
		 		"user_id"=>$this->session->userdata(S_USER_ID),
		 		"user_nik"=>$this->session->userdata(S_NIK),
		 		"user_nama_lgkp"=>$this->session->userdata(S_NAMA_LGKP),
		 		"user_nama_dpn"=>$this->session->userdata(S_NAMA_DPN),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL)
    		);
			$this->load->view('Dashboard_capil/index',$data);
	}
	public function get_data()
	{
		header('Content-type: application/json');
		 $capil = $this->dsb->get_capil();
		 $output = array_merge($capil);
        echo json_encode($output);
	}
}
