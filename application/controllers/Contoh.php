<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contoh extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('Pdf');
	}
	public function index()
	{
		$this->load->view('shared/contoh');
		
	}

}
