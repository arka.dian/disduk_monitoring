<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Activity extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->model('M_Login','lgn');
		$this->load->model('M_Activity','act');
		$this->load->model('M_Shared','shr');
		if ($this->session->userdata(S_SESSION_ID) == null) 
		{
			redirect('/','refresh');
		} else {
			$is_log = $this->shr->get_islogin($this->session->userdata(S_IP_ADDRESS),$this->session->userdata(S_USER_ID));
			if ($is_log == 0){
				if ($this->session->userdata(S_SESSION_ID) != null) {
		 		$this->shr->stop_activity($this->session->userdata(S_USER_ID));
		 		}
				$this->session->sess_destroy();
			}
		}
	}
	public function index()
	{
		
			$menu = $this->shr->get_menu($this->session->userdata(S_USER_LEVEL));
			$is_online = $this->act->is_online($this->session->userdata(S_USER_ID));
			$clock_in = $this->act->get_clockin($this->session->userdata(S_USER_ID));
			$clock_out = $this->act->get_clockout($this->session->userdata(S_USER_ID));
			$js = $this->act->get_count_activity($this->session->userdata(S_USER_ID));
			if($js > 0){
				$rs = $this->act->get_all_activity($this->session->userdata(S_USER_ID));
				$mrs = 	$this->act->get_recap_activity($this->session->userdata(S_USER_ID));
			}else{
				$rs = [];
				$mrs = [];
			}
			$jbc = $this->act->get_count_bcard($this->session->userdata(S_USER_ID));
			if ($jbc > 0 ){
				$rbc = $this->act->get_all_bcard($this->session->userdata(S_USER_ID));	
			}else{
				$rbc = [];	
			}
			$jbe = $this->act->get_count_benroll($this->session->userdata(S_USER_ID));
			if ($jbe > 0){
				$rbe = $this->act->get_all_benroll($this->session->userdata(S_USER_ID));	
			}else{
				$rbe = [];
			}
			$rekap = $this->act->get_all($this->session->userdata(S_USER_ID));
			
			$data = array(
		 		"stitle"=>'My Activity Page',
		 		"user_id"=>$this->session->userdata(S_USER_ID),
		 		"user_nik"=>$this->session->userdata(S_NIK),
		 		"menu"=>$menu,
		 		"is_online"=>$is_online,
		 		"data_siak"=>$rs,
		 		"master_data_siak"=>$mrs,
		 		"jumlah_siak"=>$js,
		 		"data_bcard"=>$rbc,
		 		"jumlah_bcard"=>$jbc,
		 		"data_benroll"=>$rbe,
		 		"jumlah_benroll"=>$jbe,
		 		"clock_in"=>$clock_in,
		 		"clock_out"=>$clock_out,
		 		"rekap"=>$rekap,
		 		"user_nama_lgkp"=>$this->session->userdata(S_NAMA_LGKP),
		 		"user_nama_kantor"=>$this->session->userdata(S_NAMA_KANTOR),
		 		"user_nama_dpn"=>$this->session->userdata(S_NAMA_DPN),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL)
    		);
			$this->load->view('Activity/index',$data);
		
	}
	public function setting()
	{
		
			$menu = $this->shr->get_menu($this->session->userdata(S_USER_LEVEL));
			$r = $this->act->get_user($this->session->userdata(S_USER_ID));
			$data = array(
		 		"stitle"=>'Account Setting',
		 		"user_id"=>$this->session->userdata(S_USER_ID),
		 		"user_nik"=>$this->session->userdata(S_NIK),
		 		"menu"=>$menu,
		 		"data"=>$r,
		 		"user_nama_lgkp"=>$this->session->userdata(S_NAMA_LGKP),
		 		"user_nama_kantor"=>$this->session->userdata(S_NAMA_KANTOR),
		 		"user_nama_dpn"=>$this->session->userdata(S_NAMA_DPN),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL)
    		);
			$this->load->view('Activity_setting/index',$data);
		
	}
	public function activity_daily()
	{
		
			$menu = $this->shr->get_menu($this->session->userdata(S_USER_LEVEL));
			if($this->input->post('user_id') != null){
			$tgl = $this->input->post('tanggal');
			$tgl_start = substr($tgl, 0, 10);
			$tgl_end = substr($tgl,13, 10);
			$user_id = $this->input->post('user_id');
			$r = $this->act->cek_daily_activity($tgl_start,$tgl_end,$user_id);
			$j = $this->act->cek_count_daily_activity($tgl_start,$tgl_end,$user_id);
			$data = array(
		 		"stitle"=>$this->input->post('user_id').' Rekap Daily Report',
		 		"mtitle"=>$this->input->post('user_id').' Rekap Daily Report',
		 		"my_url"=>'activity_daily',
		 		"type_tgl"=>'Report',
		 		"menu"=>$menu,
		 		"data"=>$r,
		 		"jumlah"=>$j,
		 		"user_id"=>$this->session->userdata(S_USER_ID),
		 		"user_nik"=>$this->session->userdata(S_NIK),
		 		"user_nama_lgkp"=>$this->session->userdata(S_NAMA_LGKP),
		 		"user_nama_dpn"=>$this->session->userdata(S_NAMA_DPN),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL),
		 		"user_no_kec"=>$this->session->userdata(S_NO_KEC)
    		);
			}else{
			$data = array(
		 		"stitle"=>'Rekap Daily Report',
		 		"mtitle"=>'Rekap Daily Report',
		 		"my_url"=>'activity_daily',
		 		"type_tgl"=>'Report',
		 		"menu"=>$menu,
		 		"user_id"=>$this->session->userdata(S_USER_ID),
		 		"user_nik"=>$this->session->userdata(S_NIK),
		 		"user_nama_lgkp"=>$this->session->userdata(S_NAMA_LGKP),
		 		"user_nama_dpn"=>$this->session->userdata(S_NAMA_DPN),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL),
		 		"user_no_kec"=>$this->session->userdata(S_NO_KEC)
    		);
    		}
			$this->load->view('activity_daily/index',$data);
		
	}
	public function absensi_daily()
	{
		
			$menu = $this->shr->get_menu($this->session->userdata(S_USER_LEVEL));
			if($this->input->post('user_id') != null){
			$user_id = $this->input->post('user_id');
			$r = $this->act->cek_daily_absensi($user_id);
			$j = $this->act->cek_count_daily_absensi($user_id);
			$data = array(
		 		"stitle"=>$this->input->post('user_id').' Rekap Daily Absensi',
		 		"mtitle"=>$this->input->post('user_id').' Rekap Daily Absensi',
		 		"my_url"=>'absensi_daily',
		 		"type_tgl"=>'Absensi',
		 		"menu"=>$menu,
		 		"data"=>$r,
		 		"jumlah"=>$j,
		 		"user_id"=>$this->session->userdata(S_USER_ID),
		 		"user_nik"=>$this->session->userdata(S_NIK),
		 		"user_nama_lgkp"=>$this->session->userdata(S_NAMA_LGKP),
		 		"user_nama_dpn"=>$this->session->userdata(S_NAMA_DPN),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL),
		 		"user_no_kec"=>$this->session->userdata(S_NO_KEC)
    		);
			}else{
			$data = array(
		 		"stitle"=>'Rekap Daily Absensi',
		 		"mtitle"=>'Rekap Daily Absensi',
		 		"my_url"=>'absensi_daily',
		 		"type_tgl"=>'Absensi',
		 		"menu"=>$menu,
		 		"user_id"=>$this->session->userdata(S_USER_ID),
		 		"user_nik"=>$this->session->userdata(S_NIK),
		 		"user_nama_lgkp"=>$this->session->userdata(S_NAMA_LGKP),
		 		"user_nama_dpn"=>$this->session->userdata(S_NAMA_DPN),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL),
		 		"user_no_kec"=>$this->session->userdata(S_NO_KEC)
    		);
    		}
			$this->load->view('absensi_daily/index',$data);
		
	}
	public function do_save(){
		header('Content-type: application/json');
		$user_name = $this->input->post('user_name');
		$old_password = md5($this->input->post('old_password'));
		$new_password = md5($this->input->post('new_password'));
		$r = $this->lgn->check_user($user_name, $old_password);
		if (count($r) > 0) {
			$r = $r[0];
			if ($r->USER_PWD == $old_password ) {
				$r = $this->lgn->change_pwd($user_name, $new_password);
				$output = array(
		    			"message_type"=>1,
		    			"message"=>"Your Password Has Been Changed Successfully! Thank you."
		    	);
			}else{
				$output = array(
		    			"message_type"=>0,
		    			"message"=>"Old Password Incorect For User $user_name Please Try Again"
		    		);
			}
		}else{
				$output = array(
	    			"message_type"=>0,
	    			"message"=>"Sorry User $user_name Not Found, Please Contact Admin For Registration"
	    		);
		}
		echo json_encode($output);
	}

}