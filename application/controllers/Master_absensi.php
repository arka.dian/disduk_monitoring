<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Master_absensi extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->model('M_Activity','act');
		$this->load->model('M_Shared','shr');
		if ($this->session->userdata(S_SESSION_ID) == null) 
		{
			redirect('/','refresh');
		} else {
			$is_log = $this->shr->get_islogin($this->session->userdata(S_IP_ADDRESS),$this->session->userdata(S_USER_ID));
			if ($is_log == 0){
				if ($this->session->userdata(S_SESSION_ID) != null) {
		 		$this->shr->stop_activity($this->session->userdata(S_USER_ID));
		 		}
				$this->session->sess_destroy();
			}
		}
	}
	public function index()
	{
			$menu = $this->shr->get_menu($this->session->userdata(S_USER_LEVEL));
			$r = $this->act->get_master_absensi();	
			$r2 = [];
			// $r2 = $this->act->get_master_absensi_dinas();
			
			$data = array(
		 		"stitle"=>'Master Absensi Page',
		 		"menu"=>$menu,
		 		"user_id"=>$this->session->userdata(S_USER_ID),
		 		"user_nik"=>$this->session->userdata(S_NIK),
		 		"data"=>$r,
		 		"data2"=>$r2,
		 		"user_nama_lgkp"=>$this->session->userdata(S_NAMA_LGKP),
		 		"user_nama_kantor"=>$this->session->userdata(S_NAMA_KANTOR),
		 		"user_nama_dpn"=>$this->session->userdata(S_NAMA_DPN),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL)
    		);
			$this->load->view('master_absensi/index',$data);
		
	}

}