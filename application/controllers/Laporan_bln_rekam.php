<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Laporan_bln_rekam extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->model('M_Shared','shr');	
		$this->load->model('M_Mastercakupan','mst');
		if ($this->session->userdata(S_SESSION_ID) == null) 
		{
			redirect('/','refresh');
		} else {
			$is_log = $this->shr->get_islogin($this->session->userdata(S_IP_ADDRESS),$this->session->userdata(S_USER_ID));
			if ($is_log == 0){
				if ($this->session->userdata(S_SESSION_ID) != null) {
		 		$this->shr->stop_activity($this->session->userdata(S_USER_ID));
		 		}
				$this->session->sess_destroy();
			}
		}
	}
	public function index()
	{
			
			$menu = $this->shr->get_menu($this->session->userdata(S_USER_LEVEL));
			if($this->input->post('bulan') != null){
			$bln = $this->input->post('bulan');
			
			$r = $this->mst->get_bln_rekam_ktp($bln);
			$j = $this->mst->get_jum_bln_rekam_ktp($bln);
			$data = array(
		 		"stitle"=>'Rekapitulasi Laporan Perekaman Bulan '.$bln,
		 		"mtitle"=>'Rekapitulasi Laporan Perekaman',
		 		"my_url"=>'Laporan_bln_rekam',
		 		"type_tgl"=>'Cetak',
		 		"data"=>$r,
		 		"jumlah"=>$j,
		 		"menu"=>$menu,
		 		"user_id"=>$this->session->userdata(S_USER_ID),
		 		"user_nik"=>$this->session->userdata(S_NIK),
		 		"user_nama_lgkp"=>$this->session->userdata(S_NAMA_LGKP),
		 		"user_nama_dpn"=>$this->session->userdata(S_NAMA_DPN),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL),
		 		"no_wil"=>'No Kec',
		 		"kode_wil"=>'Nama Kecamatan'
    		);
			}else{
			$data = array(
		 		"stitle"=>'Rekapitulasi Laporan Perekaman',
		 		"mtitle"=>'RRekapitulasi Laporan Perekaman',
		 		"my_url"=>'Laporan_bln_rekam',
		 		"type_tgl"=>'Cetak',
		 		"menu"=>$menu,
		 		"user_id"=>$this->session->userdata(S_USER_ID),
		 		"user_nik"=>$this->session->userdata(S_NIK),
		 		"user_nama_lgkp"=>$this->session->userdata(S_NAMA_LGKP),
		 		"user_nama_dpn"=>$this->session->userdata(S_NAMA_DPN),
		 		"user_level"=>$this->session->userdata(S_USER_LEVEL),
		 		"no_wil"=>'No Kec',
		 		"kode_wil"=>'Nama Kecamatan'
    		);
    		}
			$this->load->view('laporan_bln/index',$data);
	}
	
}
