<div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title"><?php echo $stitle; ?></h4> </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <button class="right-side-toggle waves-effect waves-light btn-info btn-circle pull-right m-l-20"><i class="ti-settings text-white"></i></button>
                        <ol class="breadcrumb">
                            <li class="active"><?php echo $stitle; ?></li>
                        </ol>
                    </div>
                </div>
                <!-- .row -->
               <div class="row">
                <div class="row">
                    <div class="col-md-4 col-xs-12">
                        <div class="white-box">
                            <div class="user-bg"> <img width="100%" alt="user" src="<?php echo base_url('assets/plugins/images/wallpaper2.jpg') ?>">
                                <div class="overlay-box">
                                    <div class="user-content">
                                        <a href="javascript:void(0)"> <?php $filename = 'assets/upload/pp/'.$user_nik.'.jpg';
                                            if (file_exists($filename)) { ?>
                                            <img src='<?php echo base_url('assets/upload/pp/'.$user_nik.'.jpg') ?>'  class="img-circle thumb-lg" alt="user-img" >
                                            <?php } else {?>
                                            <img src="<?php echo base_url()?>assets/plugins/images/placeholder-logo.gif" class="img-circle thumb-lg" alt="user-img">
                                        <?php }?></a>
                                        <h4 class="text-white"><?php echo $user_nama_lgkp; ?></h4>
                                        <h5 class="text-white"><?php if($user_level == 1){echo 'KECAMATAN';}?> <?php echo $user_nama_kantor; ?></h5>  </div>
                                </div>
                            </div>
                            <div class="user-btm-box">
                                <div class="col-md-6 col-sm-6 text-center">
                                    <p class="text-primary"><b> Clock In :</b></p>
                                   <h1><?php if (!empty($clock_in)){ ?><?php echo $clock_in; ?><?php }else{ ?>-<?php } ?></h1> 
                                </div>
                                <div class="col-md-6 col-sm-6 text-center">
                                    <p class="text-primary"><b> Clock Out :</b></p>
                                    <h1><?php if (!empty($clock_out)){ ?><?php echo $clock_out; ?><?php }else{ ?>-<?php } ?></h1> 
                                </div>
                            </div>
                            <div class="user-btm-box">
                                <div class="col-md-4 col-sm-4 text-center">
                                    <p class="text-warning"><b> Siak Activity</b></p>
                                    <h1><?php if (!empty($jumlah_siak)){ ?><?php echo $jumlah_siak; ?><?php }else{ ?>0<?php } ?></h1> 
                                </div>
                                <div class="col-md-4 col-sm-4 text-center">
                                    <p class="text-blue"><b> Bcard Activity</b></p>
                                    <h1><?php if (!empty($jumlah_bcard)){ ?><?php echo $jumlah_bcard; ?><?php }else{ ?>0<?php } ?></h1> 
                                </div>
                                <div class="col-md-4 col-sm-4 text-center">
                                    <p class="text-danger"><b> Benroll Activity</b></p>
                                    <h1><?php if (!empty($jumlah_benroll)){ ?><?php echo $jumlah_benroll; ?><?php }else{ ?>0<?php } ?></h1> 
                                </div>
                            </div>
                            <?php if (!empty($rekap)){ ?>
                            <?php foreach($rekap as $row){?>
                            <div class="user-btm-box">
                                <div class="col-md-4 col-sm-4 text-center">
                                    <p class="text-warning"><b> Siak Bulan Ini</b></p>
                                    <h1><?php echo $row->ALL_SIAK; ?></h1> 
                                </div>
                                <div class="col-md-4 col-sm-4 text-center">
                                    <p class="text-blue"><b> Bcard Bulan Ini</b></p>
                                    <h1><?php echo $row->ALL_CETAK; ?></h1> 
                                </div>
                                <div class="col-md-4 col-sm-4 text-center">
                                    <p class="text-danger"><b> Benroll Bulan Ini</b></p>
                                    <h1><?php echo $row->ALL_REKAM; ?></h1> 
                                </div>
                            </div>
                            <?php } ?>
                            <?php } ?>
                        </div>
                        <div class="white-box">
                            <h3><b>My User Control</b> <span class="pull-right"></span></h3>
                            <hr>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="pull-left">
                                        <address>
                                            <h3><b class="text-danger"><?php echo $user_nama_lgkp; ?></b></h3>
                                            <?php if (!empty($is_online)){ ?>
                                             <h4 class="font-bold">My Siak</h4>

                                            <p class="m-l-5">
                                                <b class="font-bold">User Id : &nbsp;<?php echo $is_online[0]->USER_SIAK; ?></b><br/>
                                                <b>Status :</b> <?php if ($is_online[0]->SIAK_IP != '-'){ ?>
                                                    &nbsp;<b class="text-success">Is Online</b>
                                                <?php }else{ ?>
                                                    &nbsp;<b class="text-danger">Is Offline</b>
                                                <?php  } ?>
                                                <br/> <b>Ip Address :</b> &nbsp;<?php echo $is_online[0]->SIAK_IP; ?>
                                                <br/> <b>Last Login :</b> <i class="fa fa-calendar"></i> &nbsp;<?php echo $is_online[0]->SIAK_LAST; ?>
                                            </p>
                                             <h4 class="font-bold">My Monitoring</h4>
                                            <p class="m-l-5">
                                                <b class="font-bold">User Id : &nbsp;<?php echo $is_online[0]->USER_ID; ?></b><br/>
                                                <b>Status :</b> <?php if ($is_online[0]->MONEV_IP != '-'){ ?>
                                                    &nbsp;<b class="text-success">Is Online</b>
                                                <?php }else{ ?>
                                                    &nbsp;<b class="text-danger">Is Offline</b>
                                                <?php  } ?>
                                                <br/> <b>Ip Address :</b> &nbsp;<?php echo $is_online[0]->MONEV_IP; ?>
                                                <br/> <b>Last Login :</b> <i class="fa fa-calendar"></i> &nbsp;<?php echo $is_online[0]->MONEV_LAST; ?>
                                            </p>
                                            <?php } ?>
                                        </address>
                                    </div>
                                    <div class="pull-left text-left">
                                        <address>
                                            <h4 class="font-bold">Rekap Activity Siak</h4>
                                            <p class="m-l-5">
                                           <?php   if (!empty($master_data_siak)){
                                            foreach($master_data_siak as $row){?>
                                                <b><?php echo $row->P1 ;?> :</b> &nbsp;<?php echo $row->JML ;?> <br/>
                                            <?php } ?>
                                            <?php }else{ ?>
                                                <b>Tidak Ada Aktivitas Siak</b> 
                                            <?php } ?>
                                            </p>
                                        </address>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-8 col-xs-12">
                        <div class="white-box">
                            <ul class="nav nav-tabs tabs customtab">
                                <li class="active tab">
                                    <a href="#siak" data-toggle="tab"> <span class="visible-xs"><i class="fa fa-desktop"></i></span> <span class="hidden-xs">Siak Activity</span> </a>
                                </li>
                                <li class="tab">
                                    <a href="#bcard" data-toggle="tab"> <span class="visible-xs"><i class="fa fa fa-credit-card"></i></span> <span class="hidden-xs">Bcard Activity</span> </a>
                                </li>
                                <li class="tab">
                                    <a href="#benroll" data-toggle="tab"> <span class="visible-xs"><i class="fa fa-camera"></i></span> <span class="hidden-xs">Benroll Activity</span> </a>
                                </li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane active" id="siak">
                                    <div class="steamline">
                            <?php   if (!empty($data_siak)){
                                    foreach($data_siak as $row){?>
                                        <div class="sl-item">
                                            <div class="sl-left">
                                        <?php $filename = 'assets/upload/pp/'.$row->NIK.'.jpg';
                                            if (file_exists($filename)) { ?>
                                            <img src='<?php echo base_url('assets/upload/pp/'.$row->NIK.'.jpg') ?>'  class="img-circle img-responsive" alt="user-img" >
                                            <?php } else {?>
                                            <img src="<?php echo base_url()?>assets/plugins/images/placeholder-logo.gif" class="img-circle" alt="user-img">
                                        <?php }?> </div>
                                            <div class="sl-right">
                                                <div class="m-l-40"> <a href="#" class="text-info"><?php echo $row->NAMA_LGKP ;?></a> <span class="sl-date">Jam <?php echo $row->ACTIVITY_DATE ;?></span>
                                                    <div class="m-t-20 row">
                                                        <div class="col-md-12 col-xs-12">
                                                            <a href="#" class="btn btn-success" style="margin-bottom: 10px !important"><?php echo $row->P1 ;?> <?php echo $row->P2 ;?></a>
                                                            <p> <?php echo $row->P3 ;?></p></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                            <?php } ?>
                            <?php }else{ ?>
                                    <div class="sl-left">
                                        <?php $filename = 'assets/upload/pp/'.$user_nik.'.jpg';
                                            if (file_exists($filename)) { ?>
                                            <img src='<?php echo base_url('assets/upload/pp/'.$user_nik.'.jpg') ?>'  class="img-circle img-responsive" alt="user-img" >
                                            <?php } else {?>
                                            <img src="<?php echo base_url()?>assets/plugins/images/placeholder-logo.gif" class="img-circle" alt="user-img">
                                        <?php }?> 
                                    </div>
                                    <div class="sl-item">
                                            <div class="sl-right">There Is No Data To Be Displayed On Your SIAK Activity</div>
                                    </div>

                            <?php } ?> 
                                    </div>
                                    </div>
                                <div class="tab-pane" id="bcard">
                                    <div class="steamline">
                            <?php   if (!empty($data_bcard)){
                                    foreach($data_bcard as $row){?>
                                        <div class="sl-item">
                                            <div class="sl-left">
                                        <?php $filename = 'assets/upload/pp/'.$row->NIK.'.jpg';
                                            if (file_exists($filename)) { ?>
                                            <img src='<?php echo base_url('assets/upload/pp/'.$row->NIK.'.jpg') ?>'  class="img-circle img-responsive" alt="user-img" >
                                            <?php } else {?>
                                            <img src="<?php echo base_url()?>assets/plugins/images/placeholder-logo.gif" class="img-circle" alt="user-img">
                                        <?php }?> </div>
                                            <div class="sl-right">
                                                <div class="m-l-40"> <a href="#" class="text-info"><?php echo $row->NAMA_LGKP ;?></a> <span class="sl-date">Jam <?php echo $row->ACTIVITY_DATE ;?></span>
                                                    <div class="m-t-20 row">
                                                        <div class="col-md-12 col-xs-12">
                                                            <a href="#" class="btn btn-info" style="margin-bottom: 10px !important"><?php echo $row->P1 ;?> <?php echo $row->P2 ;?></a>
                                                            <p> <?php echo $row->P3 ;?></p></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                            <?php } ?>
                            <?php }else{ ?>
                                    <div class="sl-left">
                                        <?php $filename = 'assets/upload/pp/'.$user_nik.'.jpg';
                                            if (file_exists($filename)) { ?>
                                            <img src='<?php echo base_url('assets/upload/pp/'.$user_nik.'.jpg') ?>'  class="img-circle img-responsive" alt="user-img" >
                                            <?php } else {?>
                                            <img src="<?php echo base_url()?>assets/plugins/images/placeholder-logo.gif" class="img-circle" alt="user-img">
                                        <?php }?> 
                                    </div>
                                    <div class="sl-item">
                                            <div class="sl-right">There Is No Data To Be Displayed On Your Biomorf Card Activity</div>
                                    </div>

                            <?php } ?> 
                                    </div>
                                    </div>
                                    <div class="tab-pane" id="benroll">
                                    <div class="steamline">
                            <?php   if (!empty($data_benroll)){
                                    foreach($data_benroll as $row){?>
                                        <div class="sl-item">
                                            <div class="sl-left">
                                        <?php $filename = 'assets/upload/pp/'.$row->NIK.'.jpg';
                                            if (file_exists($filename)) { ?>
                                            <img src='<?php echo base_url('assets/upload/pp/'.$row->NIK.'.jpg') ?>'  class="img-circle img-responsive" alt="user-img" >
                                            <?php } else {?>
                                            <img src="<?php echo base_url()?>assets/plugins/images/placeholder-logo.gif" class="img-circle" alt="user-img">
                                        <?php }?> </div>
                                            <div class="sl-right">
                                                <div class="m-l-40"> <a href="#" class="text-info"><?php echo $row->NAMA_LGKP ;?></a> <span class="sl-date">Jam <?php echo $row->ACTIVITY_DATE ;?></span>
                                                    <div class="m-t-20 row">
                                                        <div class="col-md-12 col-xs-12">
                                                            <a href="#" class="btn btn-warning" style="margin-bottom: 10px !important"><?php echo $row->P1 ;?> <?php echo $row->P2 ;?></a>
                                                            <p> <?php echo $row->P3 ;?></p></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                            <?php } ?>
                            <?php }else{ ?>
                                    <div class="sl-left">
                                        <?php $filename = 'assets/upload/pp/'.$user_nik.'.jpg';
                                            if (file_exists($filename)) { ?>
                                            <img src='<?php echo base_url('assets/upload/pp/'.$user_nik.'.jpg') ?>'  class="img-circle img-responsive" alt="user-img" >
                                            <?php } else {?>
                                            <img src="<?php echo base_url()?>assets/plugins/images/placeholder-logo.gif" class="img-circle" alt="user-img">
                                        <?php }?> 
                                    </div>
                                    <div class="sl-item">
                                            <div class="sl-right">There Is No Data To Be Displayed On Your Biomorf Enrollment Activity</div>
                                    </div>

                            <?php } ?> 
                                    </div>
                                    </div>
                            </div>
                        </div>
                    </div>
                </div>
                   
                <!-- /.row -->
                <!-- ============================================================== -->
                <!-- Right sidebar -->
                <!-- ============================================================== -->
                <!-- .right-sidebar -->
                
                <!-- ============================================================== -->
                <!-- End Right sidebar -->
                <!-- ============================================================== -->
            </div>
            <!-- /.container-fluid -->
         <?php $this->view('shared/footer_detail'); ?>