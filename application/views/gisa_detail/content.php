<div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title"><?php echo $stitle; ?></h4> </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <button class="right-side-toggle waves-effect waves-light btn-info btn-circle pull-right m-l-20"><i class="ti-settings text-white"></i></button>
                        <ol class="breadcrumb">
                            <li class="active"><?php echo $stitle; ?></li>
                        </ol>
                    </div>
                </div>
                <!-- .row -->
                <div class="row">
                    <div class="col-md-12 col-sm-12">
                    <div class="white-box">
                            <table style="width: 100% !important">
                                <tr>
                                    <td colspan="7" style="text-align: center; border: 1px solid #fff;">
                                    <b><h3 align="center" style="font-weight: bold; margin-top: 0; margin-bottom: 0;">Kartu Keluarga</h3></b>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="7" style="text-align: center; border: 1px solid #fff;">
                                    <b><h3 align="center" style="font-weight: bold; margin-top: 0;">NO. <?php echo $sno_kk; ?></h3></b>
                                    </td>
                                </tr>
                            </table>
                            <?php if (!empty($data_kk)){ 
                                foreach($data_kk as $row){
                            ?>
                            <div class="row">
                                <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
                                    <div class="row">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" style="text-align: center !important; lign-content: center !important">
                                        Alamat : <span style="font-weight: bold;"><?php echo $row->ALAMAT ;?></span>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" style="text-align: center !important; lign-content: center !important">
                                        RT/RW : <span style="font-weight: bold;"><?php echo $row->NO_RT ;?>/<?php echo $row->NO_RW ;?></span>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" style="text-align: center !important; lign-content: center !important">
                                        Desa/Kelurahan : <span style="font-weight: bold;"><?php echo $row->NAMA_KEL ;?></span>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" style="text-align: center !important; lign-content: center !important">
                                        Kecamatan : <span style="font-weight: bold;"><?php echo $row->NAMA_KEC ;?></span>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" style="text-align: center !important; lign-content: center !important">
                                        Kabupaten/Kota : <span style="font-weight: bold;"><?php echo $row->NAMA_KAB ;?></span>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" style="text-align: center !important; lign-content: center !important">
                                        Provinsi : <span style="font-weight: bold;"><?php echo $row->NAMA_PROP ;?></span>
                                    </div>
                                    </div>
                                </div>
                                <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
                                        <a href="<?php echo base_url()?>gisa?no_kec=<?php echo $row->NO_KEC ;?>&no_kel=<?php echo $row->NO_KEL ;?>&no_rw=<?php echo $row->NO_RW ;?>&no_rt=<?php echo $row->NO_RT ;?>" onclick="on_menu();"><span class="btn btn-outline btn-success btn-lg btn-block" style="margin-top: 20px !important;">Kembali</span></a>
                                </div>
                            </div>
                            <?php 
                                }
                                } 
                            ?>
                            
                        </div>
                        </div>
                        </div>
                        <div class="row">
                         <?php if (!empty($data_nik)){ 
                                foreach($data_nik as $row){
                            ?>
                            <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
                            <div class="white-box">
                            <h3><b><?php echo $row->STAT_HBKEL ;?></b> <span class="pull-right"></span></h3>
                            <hr>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="pull-left">
                                        <address>
                                            <h3><b class="text-danger"><?php echo $row->NAMA_LGKP ;?></b></h3>
                                             <h4 class="font-bold"><?php echo $row->NIK ;?></h4>

                                            <p class="m-l-5">
                                                <b>Tempat Lahir : </b>&nbsp;<span class="font-bold"><?php echo $row->TMPT_LHR ?></span><br/>
                                                <b>Tanggal Lahir : </b> <i class="fa fa-calendar"></i>&nbsp;<span class="font-bold"><?php echo $row->TGL_LHR ?> (<?php echo $row->UMUR ?> Tahun)</span><br/>
                                                <b>Jenis Kelamin : </b>&nbsp;<span class="font-bold"><?php echo $row->JENIS_KLMIN ?></span><br/>
                                                <b>Golongan Darah :</b>&nbsp;<span class="font-bold"><?php echo $row->GOL_DRH ?></span><br/> 
                                                <b>Agama :</b>&nbsp;<span class="font-bold"><?php echo $row->AGAMA ?></span><br/> 
                                                <b>Status Kawin :</b>&nbsp;<span class="font-bold"><?php echo $row->STATUS_KAWIN ?></span><br/> 
                                                <b>Pekerjaan :</b>&nbsp;<span class="font-bold"><?php echo $row->PEKERJAAN ?></span>
                                            </p>
                                             <h4 class="font-bold">Monitoring #GISA</h4>
                                            <p class="m-l-5">
                                                <b>Status KK : </b>&nbsp;<span class="font-bold"><?php echo $row->STATUS_KK ?></span><br/>
                                                <b>Status Perekaman : </b>&nbsp;<span class="font-bold"><?php echo $row->STATUS_REKAM ?></span><br/>
                                                <b>Status KTP : </b>&nbsp;<span class="font-bold"><?php echo $row->STATUS_KTP ?></span><br/>
                                                <b>Status Akta Kelahiran :</b>&nbsp;<span class="font-bold"><?php echo $row->STATUS_AKTA_LHR ?></span><br/> 
                                                <b>No Akta Kelahiran :</b>&nbsp;<span class="font-bold"><?php echo $row->NO_AKTA_LHR ?></span><br/> 
                                                <?php if($row->STATUS_AKTA_KWN != '-'){ ?>
                                                <b>Status Akta Perkawinan :</b>&nbsp;<span class="font-bold"><?php echo $row->STATUS_AKTA_KWN ?></span><br/> 
                                                <b>No Akta Perkawinan :</b>&nbsp;<span class="font-bold"><?php echo $row->NO_AKTA_KWN ?></span><br/> 
                                                <?php } ?> 
                                                <?php if($row->STATUS_AKTA_CRAI != '-'){ ?>
                                                <b>Status Akta Perceraian :</b>&nbsp;<span class="font-bold"><?php echo $row->STATUS_AKTA_CRAI ?></span><br/> 
                                                <b>No Akta Perceraian :</b>&nbsp;<span class="font-bold"><?php echo $row->NO_AKTA_CRAI ?></span><br/> 
                                                <?php } ?> 
                                                <?php if($row->STATUS_KIA != '-'){ ?>
                                                <b>Status KIA : </b>&nbsp;<span class="font-bold"><?php echo $row->STATUS_KIA ?></span>
                                                <?php } ?> 
                                            </p>
                                        </address>
                                    </div>
                                    <div class="pull-right text-right">
                                        <address>
                                           <a href="<?php echo base_url()?>gisa/do_edit?nik=<?php echo $row->NIK ;?>" onclick="on_menu();"><span  class="btn btn-success btn-sm" id="btn-detail" style="margin-top: 5px;">Ubah <i class="mdi mdi-tooltip-edit fa-fw"></i></span></a>
                                        </address>
                                    </div>
                                </div>
                            </div>
                        </div>
                            </div>
                            <?php 
                            }
                            }
                            ?>
                        </div>
                    <!-- .col -->
                    
           
                
                
                <!-- /.row -->
                <!-- ============================================================== -->
                <!-- Right sidebar -->
                <!-- ============================================================== -->
                <!-- .right-sidebar -->
                
                <!-- ============================================================== -->
                <!-- End Right sidebar -->
                <!-- ============================================================== -->
            </div>
            <!-- /.container-fluid -->
         <?php $this->view('shared/footer_detail'); ?>