 <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title"><?php echo $stitle; ?></h4> </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <button class="right-side-toggle waves-effect waves-light btn-info btn-circle pull-right m-l-20"><i class="ti-settings text-white"></i></button>
                        <ol class="breadcrumb">
                            <li class="active"><?php echo $stitle; ?></li>
                        </ol>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12 col-sm-12 col-lg-12">
                        
                           <div class="row">
                            <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                        <div class="panel">
                            <div class="p-20">
                                <div class="row">
                                    <div class="col-xs-8">
                                        <h2 class="font-medium m-t-0">List Pencetakan KTP-EL Per-User</h2>
                                        <h5 class="text-muted m-t-0">Tanggal <?php 
                                        $date = date('d-m-Y');
                                        echo $date;
                                        ?></h5>
                                    </div>
                                    <div class="col-xs-4 p-20">
                                        
                                    </div>
                                </div>
                            </div>
                            <div class="panel-footer bg-extralight">
                                <ul class="earning-box">
                                    <?php if (!empty($user_cetak)){
                                    foreach($user_cetak as $row){?>
                                    <li class="b-t b-b">
                                        <div class="er-row">
                                            <div class="er-text" >
                                                <h4 style="color: #41b3f9 !important;"><?php echo $row->USER_ID ;?></h4><span class="text-muted" style="color: #41b3f9 !important;"><?php echo $row->TGL ;?></span></div>
                                            <div class="er-count ">~ <span class="counter"><?php echo $row->JML ;?></span></div>
                                        </div>
                                    </li>
                                    <?php } ?>
                                    <?php }else{ ?>
                                     <li>
                                        <div class="er-row">
                                            <div class="er-text b-t b-r">
                                                <h4>Petugas Cetak Belum Ada Yang Mencetak Ktp-El Hari Ini</h4><span class="text-muted"><?php 
                                                    $date = date('d-m-Y');
                                                    echo $date;
                                                    ?></span></div>
                                            <div class="er-count ">~ <span class="counter">0</span></div>
                                        </div>
                                    </li>

                                    <?php } ?>
                             
                                </ul>
                            </div>
                        </div>
                        </div>
                    </div>
                    </div>
                 
                </div>

                 </div>
       <?php $this->view('shared/footer_detail'); ?>